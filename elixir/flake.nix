{
  description = "";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/release-23.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs =
    { self
    , nixpkgs
    , flake-utils
    ,
    }:
    flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs {
        inherit system;
      };

      erlang = pkgs.erlang_26;
      elixir = (pkgs.beam.packagesWith erlang).elixir_1_16;
    in
    {
      devShell = pkgs.mkShell {
        buildInputs = [
          elixir
          pkgs.elixir_ls
          pkgs.just
        ];
      };
      formatter = pkgs.nixpkgs-fmt;
    });
}
